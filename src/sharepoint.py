from shareplum import Site, Office365
from requests_ntlm import HttpNtlmAuth
from shareplum.site import Version
from dotenv import load_doten
import os
from io import BytesIO
import openpyxl

load_dotenv()
_envs=dict(os.environ)

USERNAME = _envs["USERNAME"]
PASSWD = _envs["PASSWD"]
element_to_find = "toto"


authcookie = Office365('https://alteca.sharepoint.com/', username=USERNAME, password=PASSWD).GetCookies()
site = Site('https://alteca.sharepoint.com/sites/OrganisationDatafab/', authcookie=authcookie)

folder_to_search_in = 'Documents partages/Forms/AllItems.aspx'

def search_string_in_excel(file_content, target):
    try:
        workbook = openpyxl.load_workbook(file_content)
        for sheet in workbook.sheetnames:
            for row in workbook[sheet].iter_rows(values_only=True):
                for cell_value in row:
                    if target in str(cell_value):
                        return True
        return False
    except Exception as e:
        print(f"Error reading Excel file: {e}")
        return False
t
def search_excel_files_in_sharepoint(sharepoint_site, folder_to_search_in, element_to_find):
    excel_files = []
    total_size = 0

    try:
        site = sharepoint_site.GetSiteCollectionRootFolder()
        folder = site.Folder(folder_to_search_in)
        files = folder.files
        for file in files:
            file_url = file.ServerRelativeUrl
            file_response = site.getFileByServerRelativePath(file_url).read()
            file_content = BytesIO(file_response)
            
            if search_string_in_excel(file_content, element_to_find):
                excel_files.append(file_url)
                total_size += len(file_response)

        return excel_files, total_size

    except Exception as e:
        print(f"Error searching Excel files in SharePoint: {e}")
        return [], 0

excel_files, total_size = search_excel_files_in_sharepoint(site, folder_to_search_in, element_to_find)